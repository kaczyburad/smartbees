class User{
	loadAllUsers(){
		return fetch('https://jsonplaceholder.typicode.com/users')
			.then(response =>{
				if (response.ok){
					return response.json()
				}else{
					return null
				}
		});
	}
	
	getDomain(userData){
		let email = userData['email'];
		return email.split('@')[1];
	}
	
	addPersonData(userData){
		let div = document.createElement("div");
		let qrCanvas = document.createElement("canvas");
		let paragraph = document.createElement("pre");
		paragraph.textContent = this.getPersonData(userData);
		
		
		qrCanvas.setAttribute("id", "canvas_" + userData['id']);
		QRCode.toCanvas(qrCanvas,JSON.stringify(userData), function(error){
			if (error) console.error(error)
		})
		
		div.appendChild(paragraph);
		div.appendChild(qrCanvas);
		div.classList.add("userData");
		div.classList.add("qr")
		document.getElementById("task_3").appendChild(div);
	}
	addDomainDiv(domain){
		let div = document.createElement("div");
		let paragraph = document.createElement("pre");
		paragraph.textContent = domain;
		div.appendChild(paragraph);
		div.classList.add("userData");
		div.classList.add("domain");
		document.getElementById("task_1_and_2").appendChild(div);
	}
	getPersonData(userData){
		delete userData["id"];
		let data = JSON.stringify(userData, undefined, 2);
		data = data.replaceAll('{', "");
		data = data.replaceAll('}', "");
		data = data.replaceAll('"', "");
		data = data.replaceAll(',', "")
		return data;
	}
	addFormAttr(domain){
		let hiddenInput = document.createElement("input");
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('value', domain)
		hiddenInput.setAttribute('name', 'domain[]')
		document.getElementById("form").appendChild(hiddenInput);
	}
}


let users = new User();
users.loadAllUsers().then(response =>{
	if (response == null){
		document.write("An error occured");
	}else{
		Object.keys(response).forEach(function(key){
			users.addDomainDiv(users.getDomain(response[key]));
			users.addPersonData(response[key]); 
			users.addFormAttr(users.getDomain(response[key]));
		})
	}
});