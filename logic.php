<?php
$servername = "localhost";
$username = "username";
$password = "password"; 
$databasename = "task4";

if (isset($_POST["data"])){
    $error = false;
	$conn = connectToDataBase();
    if ($conn != null){
        if (createAndSelectDataBase($conn)){
            if(!insertToDataBase($conn)){
                $error = true;
            }
        }else{
            $error = true;
        }
    }else{
        $error = true;
    }
    showToast($error);
}
function showToast($error){
    //show html alert with message if data saved successfully or not
    
    $message = 'Successfully saved all data!';
    if ($error){
        $message = 'Some error occured while saving data. Try again later';
    }
    echo "<script type='text/javascript'>alert('$message');</script>";
}

function insertToDataBase($conn){
    /* insert all domains to database, 
    if domain repeats number of repeats increases by 1 */
    
    $domains = $_POST['domain'];
	foreach ($domains as $key => $value){
		$query = "INSERT INTO domains (domain, count)
		VALUES ('".$value."', 1)
		ON DUPLICATE KEY UPDATE count = count + 1
		";
		if (!mysqli_query($conn, $query))
            return false;
	}
    return true;
}

function createAndSelectDataBase($conn){
    /* create database and table if not exists 
    and selects database to work with */
    
    global $databasename;
    $query = 'CREATE DATABASE IF NOT EXISTS '. $databasename;
	if (!mysqli_query($conn, $query))
        return false;
	
	if (!mysqli_select_db($conn, $databasename))
        return false;
	
	$query = "CREATE TABLE IF NOT EXISTS domains(
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	domain VARCHAR(30) NOT NULL,
	count INT(6),
	UNIQUE KEY(domain)
	)"; 
	if (!mysqli_query($conn, $query))
        return false;
    return true;
}

function connectToDataBase(){
    // connects to data base
    
    global $servername, $username, $password;
    $conn = new mysqli($servername, $username, $password);
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
        return null;
	}
    return $conn;
}
?>
